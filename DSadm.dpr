program DSadm;

uses
  Forms,
  data in 'data.pas' {dm: TDataModule},
  fmAdmin in 'fmAdmin.pas' {frmAdmin},
  fmFTP in 'fmFTP.pas' {frmFTP},
  fmFTP2 in 'fmFTP2.pas' {frmFTP2};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'DS Admin';
  Application.CreateForm(TfrmAdmin, frmAdmin);
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TfrmFTP, frmFTP);
  Application.CreateForm(TfrmFTP2, frmFTP2);
  Application.Run;
end.
