unit data;

interface

uses
  SysUtils, Classes, DB, ADODB, Dialogs;

type
  Tdm = class(TDataModule)
    db: TADOConnection;
    dscConfig: TDataSource;
    tblConfig: TADOTable;
    qry: TADOQuery;
    dsc: TDataSource;
    dscNews: TDataSource;
    tblNews: TADOTable;
    dscQ: TDataSource;
    tblQ: TADOTable;
    dscQF: TDataSource;
    tblQF: TADOTable;
    tblNewsClientID: TIntegerField;
    tblNewsTerminalID: TIntegerField;
    tblNewsHeadLine: TWideStringField;
    tblNewsDesc: TWideStringField;
    tblNewsStartDate: TDateTimeField;
    tblNewsStopDate: TDateTimeField;
    tblNewsActive: TBooleanField;
    tblNewsCUser: TWideStringField;
    tblNewsCDate: TDateTimeField;
    tblQQueue_no: TWideStringField;
    tblQClientID: TIntegerField;
    tblQTerminalID: TIntegerField;
    tblQQName: TWideStringField;
    tblQDesc: TWideStringField;
    tblQStartDate: TDateTimeField;
    tblQStopDate: TDateTimeField;
    tblQActive: TBooleanField;
    tblQCUser: TWideStringField;
    tblQCDate: TDateTimeField;
    tblQFClientID: TIntegerField;
    tblQFTerminalID: TIntegerField;
    tblQFFileName: TWideStringField;
    tblQFFileType: TWideStringField;
    tblQFFileSize: TIntegerField;
    tblQFFileLocation: TWideStringField;
    tblQFActive: TBooleanField;
    tblQFCUser: TWideStringField;
    tblQFCDate: TDateTimeField;
    tblConfigID: TAutoIncField;
    tblConfigClientID: TIntegerField;
    tblConfigTerminalID: TIntegerField;
    tblConfigshow_news: TBooleanField;
    tblConfigshow_ads: TBooleanField;
    tblConfigDefault_video: TWideStringField;
    tblConfigvideo_directory: TWideStringField;
    tblConfigvideo_program: TWideStringField;
    tblConfignews_program: TWideStringField;
    tblConfighttp_server: TWideStringField;
    qryClearQ: TADOQuery;
    qryGetActiveQ: TADOQuery;
    tblQFQID: TIntegerField;
    tblConfigrefresh_time: TIntegerField;
    tblConfigftp_server: TWideStringField;
    tblConfigftp_port: TIntegerField;
    tblConfigftp_user: TWideStringField;
    tblConfigftp_pass: TWideStringField;
    qryClearQF: TADOQuery;
    qryGetActiveFile: TADOQuery;
    tblQFOrdering: TIntegerField;
    tblQFAvail: TBooleanField;
    qryClearNews: TADOQuery;
    tblNewsID: TIntegerField;
    tblQID: TIntegerField;
    tblQFID: TIntegerField;
    tblConfigftp_path: TWideStringField;
    tblConfigftp_passive: TBooleanField;
    dscPlayQ: TDataSource;
    tblPlayQ: TADOTable;
    qryClearPlay: TADOQuery;
    tblPlayQID: TIntegerField;
    tblConfigdebug_mode: TBooleanField;
    tblQFNewFileName: TWideStringField;
    tblConfigsys_pwd: TWideStringField;
    qryAnotherActiveQ: TADOQuery;
    tblQRunning: TBooleanField;
    dscBanner: TDataSource;
    tblBanner: TADOTable;
    tblBannerID: TIntegerField;
    tblBannerClientID: TIntegerField;
    tblBannerTerminalID: TIntegerField;
    tblBannerFileName: TWideStringField;
    tblBannerNewFileName: TWideStringField;
    tblBannerShowType: TIntegerField;
    tblBannerShowPosition: TIntegerField;
    tblBannerFileSize: TIntegerField;
    tblBannerFileLocation: TWideStringField;
    tblBannerStartDate: TDateTimeField;
    tblBannerStopDate: TDateTimeField;
    tblBannerActive: TBooleanField;
    tblBannerCUser: TWideStringField;
    tblBannerCDate: TDateTimeField;
    tblConfignews_font: TWideStringField;
    tblConfignews_size: TIntegerField;
    tblConfignews_color: TIntegerField;
    tblConfignews_isbold: TIntegerField;
    tblConfignews_isitalic: TIntegerField;
    tblConfignews_isunderline: TIntegerField;
    tblConfigbanner_mode: TIntegerField;
    tblConfigbanner_show_interval: TIntegerField;
    qryClearBanner: TADOQuery;
    tblConfignews_delay: TIntegerField;
    tblConfignews_height: TIntegerField;
    tblConfigbanner_width: TIntegerField;
    tblConfignews_back_color: TIntegerField;
    tblConfignews_step: TIntegerField;
    tblConfigbanner_prop: TBooleanField;
    tblConfigshow_clock: TBooleanField;
    tblConfigrss_active: TBooleanField;
    tblConfigrss_url: TWideStringField;
    tblQIsMonday: TBooleanField;
    tblQIsTuesday: TBooleanField;
    tblQIsWednesday: TBooleanField;
    tblQIsThursday: TBooleanField;
    tblQIsFriday: TBooleanField;
    tblQIsSaturday: TBooleanField;
    tblQIsSunday: TBooleanField;
    tblQStartInterval: TDateTimeField;
    tblQEndInterval: TDateTimeField;
    tblConfiglogo_width: TIntegerField;
    tblConfiglogo_stretch: TBooleanField;
    tblConfiglogo_ratio: TBooleanField;
    tblQFFSizeMB: TFloatField;
    qryDupQueue: TADOQuery;
    tblConfigdef_type: TIntegerField;
    tblConfigdef_file: TWideStringField;
    tblHTMLUrl: TADOTable;
    tblConfigQID: TIntegerField;
    tblConfightml_show: TBooleanField;
    tblConfightml_interval: TIntegerField;
    dscHTMLUrl: TDataSource;
    tblConfightml_pos: TWideStringField;
    tblConfightml_size: TIntegerField;
    tblHTMLUrlQID: TIntegerField;
    tblHTMLUrlURL: TWideStringField;
    tblHTMLUrlOrdering: TIntegerField;
    tblHTMLConfig: TADOTable;
    tblHTMLConfigID: TAutoIncField;
    tblHTMLConfigQID: TIntegerField;
    tblHTMLConfigattribute: TWideStringField;
    tblHTMLConfigdata: TWideStringField;
    dscHTMLConfig: TDataSource;
    qryClearHTML: TADOQuery;
    tblHTMLUrlID: TIntegerField;
    tblHTMLUrlCID: TIntegerField;
    tblHTMLUrlInterval: TIntegerField;
    procedure dscQStateChange(Sender: TObject);
    procedure dscQFStateChange(Sender: TObject);
    procedure dscNewsStateChange(Sender: TObject);
    procedure tblQFCalcFields(DataSet: TDataSet);
    procedure dscBannerStateChange(Sender: TObject);
    procedure dscConfigStateChange(Sender: TObject);
    procedure dscHTMLUrlStateChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure RefreshPlayQ;
    procedure FindPlayQIdx(idx: integer);
  end;

var
  dm: Tdm;

implementation

uses
  fmAdmin, DateUtils;

{$R *.dfm}


procedure Tdm.RefreshPlayQ;
begin
  with tblPlayQ do
  begin
    if active then close;
    open;
  end;

  with tblQF do
  begin
    if active then close;
    open;
  end;
end;

procedure Tdm.FindPlayQIdx(idx: integer);
var
  i: integer;
begin     // start the first record call idx = 0
  with tblPlayQ do
  begin
    first;

    for i := 1 to idx do
    begin
      next;
    end;

  end;
end;

procedure Tdm.dscQStateChange(Sender: TObject);
var bState:boolean;
begin
  if frmAdmin.Showing then begin
    if (not bRefresh) then begin
      bState:=(tblQ.State in [dsEdit, dsInsert]);
      frmAdmin.SMDBGrid2.Enabled := not bState;
      frmAdmin.btnCreateQ.Enabled:= not bState;
      frmAdmin.btnDelQ.Enabled:=not bState;
      frmAdmin.btnEditQ.Enabled:=bState;
      frmAdmin.btnCancelQ.Enabled:=bState;
      iLastQueue:=tblQ.FieldByName('id').AsInteger;
      frmAdmin.txtTestValue1.Text:=inttostr(iLastQueue);
//      frmAdmin.txtTestValue2.Text:=booltostr(bState);
    end;
  end;
end;

procedure Tdm.dscQFStateChange(Sender: TObject);
var bState:boolean;
begin
  if frmAdmin.Showing then begin
    if (not bRefresh) then begin
      bState:=(tblQF.State in [dsEdit, dsInsert]);
      frmAdmin.SMDBGrid1.Enabled := not bState;
      frmAdmin.btnCreateF.Enabled:= not bState;
      frmAdmin.btnDelF.Enabled:=not bState;
      frmAdmin.btnEditF.Enabled:=bState;
      frmAdmin.btnCancelQF.Enabled:=bState;
      iLastFile:=tblQF.FieldByName('id').AsInteger;
      frmAdmin.txtTestValue4.Text:=inttostr(iLastFile);
//      frmAdmin.txtTestValue2.Text:=booltostr(bState);
    end;
  end;
end;

procedure Tdm.dscNewsStateChange(Sender: TObject);
var bState:boolean;
begin
  if frmAdmin.Showing then begin
    if (not bRefresh) then begin
      bState:=(tblNews.State in [dsEdit, dsInsert]);
      frmAdmin.dbgNews.Enabled := not bState;
      frmAdmin.btnCreateNews.Enabled:= not bState;
      frmAdmin.btnDelNews.Enabled:=not bState;
      frmAdmin.btnEditNews.Enabled:=bState;
      frmAdmin.btnCancelNews.Enabled:=bState;
      if (tblNews.State = dsEdit) then begin
        iLastFeed:=tblNews.FieldByName('id').AsInteger;
        frmAdmin.txtTestValue3.Text:=inttostr(iLastBanner);
      end;
//      frmAdmin.txtTestValue2.Text:=booltostr(bState);
    end;
  end;

end;

procedure Tdm.tblQFCalcFields(DataSet: TDataSet);
begin
  with DataSet do
  begin
    FieldByName('FSizeMB').AsFloat := fieldbyname('FileSize').AsInteger /1024 /1024;
  end;

end;

procedure Tdm.dscBannerStateChange(Sender: TObject);
var bState:boolean;
begin
  if frmAdmin.Showing then begin
    if (not bRefresh) then begin
      bState:=(tblBanner.State in [dsEdit, dsInsert]);
      frmAdmin.dbgImage.Enabled := not bState;
      frmAdmin.btnNewBanner.Enabled:= not bState;
      frmAdmin.btnDelBanner.Enabled:=not bState;
      frmAdmin.btnEditBanner.Enabled:=bState;
      frmAdmin.btnCancelBanner.Enabled:=bState;
      iLastBanner:=tblBanner.FieldByName('id').AsInteger;
      frmAdmin.txtTestValue2.Text:=inttostr(iLastBanner);

//      frmAdmin.txtTestValue2.Text:=booltostr(bState);
    end;
  end;

end;


procedure Tdm.dscConfigStateChange(Sender: TObject);
var bState:boolean;
begin
  if frmAdmin.Showing then begin
    if (not bRefresh) then begin
      bState:=(tblConfig.State in [dsEdit, dsInsert]);
      frmAdmin.btnSaveConf.Enabled:=bState;
      frmAdmin.btnCancelConf.Enabled:=bState;
      frmAdmin.btnRssSave.Enabled:=bState;
      frmAdmin.btnHTMLConfigSave.Enabled:=bState;
      frmAdmin.btnHTMLConfigCancel.Enabled:=bState;
      frmAdmin.btnConfigSave.Enabled:=bState;
      frmAdmin.btnConfigUpdate.Enabled:=bState;

//      frmAdmin.txtTestValue2.Text:=booltostr(bState);
    end;
  end;
end;


procedure Tdm.dscHTMLUrlStateChange(Sender: TObject);
var bState:boolean;
begin
  if frmAdmin.Showing then begin
    if (not bRefresh) then begin
      frmAdmin.btnHTMLView.Enabled:=true;
      bState:=(dm.tblHTMLUrl.State in [dsEdit, dsInsert]);
      frmAdmin.dbgHTML.Enabled := not bState;
      frmAdmin.btnHTMLAdd.Enabled:=not bState;
      frmAdmin.btnHTMLDel.Enabled:=not bState;
      frmAdmin.btnHTMLSave.Enabled:=bState;
      frmAdmin.btnHTMLCancel.Enabled:=bState;
//      frmAdmin.btnCreateNews.Enabled:= not bState;
//      frmAdmin.btnDelNews.Enabled:=not bState;
//      frmAdmin.btnEditNews.Enabled:=bState;
//      frmAdmin.btnCancelNews.Enabled:=bState;
//      if (tblNews.State = dsEdit) then begin
//        iLastFeed:=tblNews.FieldByName('id').AsInteger;
//        frmAdmin.txtTestValue3.Text:=inttostr(iLastBanner);
//      end;
//      frmAdmin.txtTestValue2.Text:=booltostr(bState);
    end;
  end;

end;

end.
