unit fmFTP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FileCtrl, AdvGlassButton, RXCtrls, ExtCtrls, DB,
  AdvOfficePager, DBCtrls, Mask, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdFTP, ComCtrls, IdAntiFreezeBase,
  IdAntiFreeze;

type
  TfrmFTP = class(TForm)
    AdminPage: TAdvOfficePager;
    AdvOfficePage2: TAdvOfficePage;
    Bevel3: TBevel;
    RxLabel12: TRxLabel;
    RxLabel13: TRxLabel;
    lblSize: TRxLabel;
    txtFile: TEdit;
    txtFileSize: TEdit;
    btnFile: TAdvGlassButton;
    btnUpload: TAdvGlassButton;
    OpenDialog1: TOpenDialog;
    RxLabel1: TRxLabel;
    btnCancel: TAdvGlassButton;
    FTP: TIdFTP;
    txtOrder: TEdit;
    ProgressBar1: TProgressBar;
    IdAntiFreeze1: TIdAntiFreeze;
    txtRealSize: TEdit;
    txtFileName: TEdit;
    procedure btnFileClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnUploadClick(Sender: TObject);
    procedure FTPWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure FTPWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetButton(value: boolean);
  end;

var
  frmFTP: TfrmFTP;
  bVideoUpload:boolean;

implementation

uses
  fmAdmin, data;
{$R *.dfm}

procedure TfrmFTP.SetButton(value: boolean);
begin
  btnFile.Enabled := Value;
  btnUpload.Enabled := Value;
  btnCancel.Enabled := True;
end;

procedure TfrmFTP.btnCancelClick(Sender: TObject);
begin
  if (bVideoUpload) then begin
    ftp.DisconnectSocket;
  end else begin
    Close;
  end;
end;

procedure TfrmFTP.FormShow(Sender: TObject);
begin
  SetButton(True);
  txtFile.Text := '';
  txtFileSize.Text := '';
  txtOrder.Text := '0';
  ProgressBar1.Position := 0;
  bVideoUpload:=false;
end;

procedure TfrmFTP.btnFileClick(Sender: TObject);
var
  myFile: file of Byte;
  myFileSize: int64;
begin
  if OpenDialog1.Execute then begin
    AssignFile(myFile, OpenDialog1.FileName);
    Reset(myFile);
    txtFile.Text := OpenDialog1.FileName;
    txtFileName.Text:=ExtractFileName(txtFile.Text);
    txtRealSize.Text := IntToStr(FileSize(myFile));
    myFileSize:=FileSize(myFile);

    if (int(myFileSize/1024/1024/1024) > 0) then begin
      lblSize.Caption:='GB';
      txtFileSize.Text:=FormatFloat('#,##0.000',FileSize(myFile)/1024/1024/1024);
      txtFileSize.Color:=clRed;
      txtFileSize.Font.Color:=clWhite;
    end else begin
      if (int(myFileSize/1024/1024) > 0) then begin
        lblSize.Caption:='MB';
        txtFileSize.Text:=FormatFloat('#,##0.000',FileSize(myFile)/1024/1024);
        txtFileSize.Color:=clYellow;
        txtFileSize.Font.Color:=clBlue;
        if (int(myFileSize/1024/1024) > 200) then begin
          txtFileSize.Color:=clRed;
          txtFileSize.Font.Color:=clWhite;
        end;
      end else begin
        lblSize.Caption:='KB';
        txtFileSize.Text:=FormatFloat('#,##0.000',FileSize(myFile)/1024);
        txtFileSize.Color:=clWhite;
        txtFileSize.Font.Color:=clBlack;
      end;
    end;

    ProgressBar1.Max := FileSize(myFile);

    CloseFile(myFile);
    txtOrder.SetFocus;
  end;
end;

procedure TfrmFTP.btnUploadClick(Sender: TObject);
var
  activestr: string;
  cmdstr: string;
  fname, flocation: string;

begin
  if txtFile.Text = '' then begin
    MessageDlg('Error: Please select video file!!!', mtError, [mbOK], 0);
    abort;
  end;
  if not FileExists(txtFile.Text) then begin
    MessageDlg('Error: File is not exist!!!', mtError, [mbOK], 0);
    abort;
  end;

  Selectnext(ActiveControl, true, true);
  SetButton(False);
  //Self.Enabled := False;

  with dm.tblQF do  begin
    MemoryStream.Clear;

    cmdstr := frmAdmin.GenHTTPRequest(true, 6,
                    dm.tblQ.fieldbyname('id').AsString
                    + '&filename=' + frmAdmin.ReplaceSpace(ExtractFileName(txtFile.Text))
                    + '&filesize=' + txtRealSize.Text
                    + '&ordering=' + txtOrder.Text
                    + '&active=Y');

    try
      frmAdmin.IdHTTP1.get(cmdstr, MemoryStream);
      Application.ProcessMessages;

      MemoryStream.Position := 0;
      frmAdmin.Memo2.Lines.Clear;
      frmAdmin.Memo2.Lines.LoadFromStream(MemoryStream);
      Application.ProcessMessages;

    except
      MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
      frmAdmin.ShowLog('Error: Cannot Add File to Server. ' + ExtractFileName(txtFile.Text));
      abort;
    end;

    ResponseStr := frmAdmin.ExtractResponse(frmAdmin.Memo2.Lines[0]);

    if strtoint(ResponseStr[0]) > 0 then begin
      flocation := ResponseStr[1];
      fname := ResponseStr[2];

      // ============start FTP part===================
      FTP.Host := ftp_server;
      FTP.Port := ftp_port;
      FTP.Username := ftp_user;
      FTP.Password := ftp_pass;

      try
        FTP.Passive := ftp_passive;
        FTP.Connect(TRUE, 60);
        Application.ProcessMessages;

        FTP.Put(txtFile.Text, flocation + '/' + fname);       // set file name which get from server
        FTP.Disconnect;

        Application.ProcessMessages;
        MessageDlg('FTP Upload Video finished.', mtInformation, [mbOK], 0);
        frmAdmin.ShowLog('Finish: Upload File to Server. ' + flocation
          + '/' + ExtractFileName(txtFile.Text) + ' (' + fname + ')');
        Application.ProcessMessages;

        // sending ftp confirmation message to server
        MemoryStream.Clear;
        cmdstr := frmAdmin.GenHTTPRequest(true, 12, fname);

        try
          frmAdmin.IdHTTP1.get(cmdstr, MemoryStream);
          Application.ProcessMessages;
          frmAdmin.ShowLog('Finish: Upload Confirmation. ' + flocation + '/'
            + ExtractFileName(txtFile.Text) + ' (' + fname + ')');
        except
          MessageDlg('Error: FTP upload video confirmation failed !!!', mtError, [mbOK], 0);
          frmAdmin.ShowLog('Error!: Upload Confirmation Failed. ' + flocation + '/'
            + ExtractFileName(txtFile.Text) + ' (' + fname + ')');
          abort;
        end;

      // finish sending ftp confirmation block
      // ================================================================================


      except on E: Exception do
        begin
          if FTP.Connected then FTP.Disconnect;
          Application.ProcessMessages;
          MessageDlg('Error: Cannot connect to server !!!', mtError, [mbok], 0);
          Application.ProcessMessages;
          frmAdmin.ShowLog('Error: Cannot FTP to Server. ' + flocation + '/'
            + ExtractFileName(txtFile.Text) + ' (' + fname + ')');
          Application.ProcessMessages;
        end;
      end;
    end
    else
    begin
      MessageDlg('Error: Upload request - response failed !!!', mtInformation, [mbOK], 0);
      frmAdmin.ShowLog('Error: Upload Request - response error. Upload file abort. ' + flocation + '/' + ExtractFileName(txtFile.Text));
    end;
  end;

  frmAdmin.GetFileListofAllQ;
  frmFTP.Close;
  //Self.Enabled := True;
end;

procedure TfrmFTP.FTPWork(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  ProgressBar1.Position := AWorkCount;
  Application.ProcessMessages;
end;

procedure TfrmFTP.FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCountMax: Integer);
begin
  ProgressBar1.Position := 0;
  Application.ProcessMessages;
end;

procedure TfrmFTP.FTPWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
begin
  ProgressBar1.Position := 0;
  Application.ProcessMessages;

end;

end.
