unit mlics;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, Math, Registry;

  function GetHardDiskSerial: string;
  function IsRegistered: Boolean;
  procedure DoRegister(MCode, InputKey: string);

const
  ProjectCode = 'DSX';
  
var
  Num1: string;
  Num2: integer;
  Registered: Boolean;

implementation

function ConvertHexBin(Input: String; IsHex: Boolean): String;
const
  Hexy = '0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F';
  Biny = '0000,0001,0010,0011,0100,0101,0110,0111,1000,1001,1010,1011,1100,1101,1110,1111';
var
  i, idx: integer;
  Temp: String;
  HexyLst, BinyLst: TStringList;
begin
  HexyLst := TStringList.Create;
  BinyLst := TStringList.Create;
  HexyLst.CommaText := Hexy;
  BinyLst.CommaText := Biny;
  Temp := '';

  if IsHex then
  begin
    for i := 1 to Length(Input) do
    begin
      idx := HexyLst.IndexOf(Input[i]);

      if idx > -1 then
        Temp := Temp + BinyLst.Strings[idx]
      else
      begin
        Result := 'Error';
        Exit;
      end;
    end;

    Result := Temp;
  end
  else
  begin
    for i := 0 to floor(Length(Input) / 4) - 1 do
    begin
      idx := BinyLst.IndexOf(Input[i*4+1] + Input[i*4+2]
               + Input[i*4+3] + Input[i*4+4]);

      if idx > -1 then
        Temp := Temp + HexyLst.Strings[idx]
      else
      begin
        Result := 'Error';
        Exit;
      end;
    end;

    Result := Temp;
  end;

  HexyLst.Free;
  BinyLst.Free;
end;

function BinarytoDecimal(Value: string): double;
var
  i: integer;
begin
  result := 0;

  for i := 1 to 8 do
  begin
    if Value[i] = '1' then result := result + power(2, 8 - i);
  end;
end;

function GetHardDiskSerial: string;
var
  NotUsed:     DWORD;
  VolumeFlags: DWORD;
  VolumeSerialNumber: DWORD;
begin
  GetVolumeInformation(PChar('c:\'), nil, 20, @VolumeSerialNumber,
                       NotUsed, VolumeFlags, nil, 0);
  Result := Format('%8.8X', [VolumeSerialNumber])
end;

function ConvertProjectNametoCode(value: string): integer;
var
  i: integer;
begin
  result := 0;
  for i := 1 to Length(value) do
  begin
    result := result + ( Ord(value[i]) * (((7 - i) * (i + 2)) + i) );
  end;
end;

function GenSerial(Value, sProject: string): string;
var
  Bint1, Bint2, Bint3: string;
  Tmp1, Tmp2, TmpProjectCode: string;
  i: integer;
begin
  Bint1 := '';
  Bint2 := '';
  Bint1 := ConvertHexBin(Value, True);

  Bint3 := '';
  for i := 1 to 5 do Bint3 := Bint3 + Bint1[i];
  Bint2 := '0' + Bint3 + '0';

  Bint3 := '';
  for i := 6 to 11 do Bint3 := Bint3 + Bint1[i];
  Bint2 := Bint2 + Bint3 + '01';

  Bint3 := '';
  for i := 12 to 17 do Bint3 := Bint3 + Bint1[i];
  Bint2 := Bint2 + Bint3 + '10';

  Bint3 := '';
  for i := 18 to 27 do Bint3 := Bint3 + Bint1[i];
  Bint2 := Bint2 + Bint3 + '0';

  Bint3 := '';
  for i := 28 to 32 do Bint3 := Bint3 + Bint1[i];
  Bint2 := Bint2 + Bint3 + '1';

  Tmp1 := '';
  for i := 0 to 4 do
  begin
    Tmp1 := Tmp1 +
      floattostr(BinarytoDecimal
                (Bint2[8*i+1] + Bint2[8*i+2] + Bint2[8*i+3] + Bint2[8*i+4]
               + Bint2[8*i+5] + Bint2[8*i+6] + Bint2[8*i+7] + Bint2[8*i+8]));
  end;

  Tmp2 := '';
  for i := 1 to Length(Tmp1) do Tmp2 := Tmp1[i] + Tmp2;  // reverse string;

  TmpProjectCode := inttostr(ConvertProjectNametoCode(sProject));

  Result := TmpProjectCode + '-'
              + TmpProjectCode[3] + TmpProjectCode[1] + Tmp2
              + TmpProjectCode[2];
end;

function GetRegistryData(RootKey: HKEY; Key, Value: string): variant;
var
  Reg: TRegistry;
  RegDataType: TRegDataType;
  DataSize, Len: integer;
  s: string;
  label cantread;
begin
  Reg := nil;
  try
    Reg := TRegistry.Create(KEY_QUERY_VALUE);
    Reg.RootKey := RootKey;
    if Reg.OpenKeyReadOnly(Key) then begin
      try
        RegDataType := Reg.GetDataType(Value);
        if (RegDataType = rdString) or
           (RegDataType = rdExpandString) then
          Result := Reg.ReadString(Value)
        else if RegDataType = rdInteger then
          Result := Reg.ReadInteger(Value)
        else if RegDataType = rdBinary then begin
          DataSize := Reg.GetDataSize(Value);
          if DataSize = -1 then goto cantread;
          SetLength(s, DataSize);
          Len := Reg.ReadBinaryData(Value, PChar(s)^, DataSize);
          if Len <> DataSize then goto cantread;
          Result := s;
        end else
          cantread:
          raise Exception.Create(SysErrorMessage(ERROR_CANTREAD));
      except
        s := ''; // Deallocates memory if allocated
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
      raise Exception.Create(SysErrorMessage(GetLastError));
  except
    Reg.Free;
    raise;
  end;
  Reg.Free;
end;

procedure SetRegistryData(RootKey: HKEY; Key, Value: string;
  RegDataType: TRegDataType; Data: variant);
var
  Reg: TRegistry;
  s: string;
begin
  Reg := TRegistry.Create(KEY_WRITE);
  try
    Reg.RootKey := RootKey;
    if Reg.OpenKey(Key, True) then begin
      try
        if RegDataType = rdUnknown then
          RegDataType := Reg.GetDataType(Value);
        if RegDataType = rdString then
          Reg.WriteString(Value, Data)
        else if RegDataType = rdExpandString then
          Reg.WriteExpandString(Value, Data)
        else if RegDataType = rdInteger then
          Reg.WriteInteger(Value, Data)
        else if RegDataType = rdBinary then begin
          s := Data;
          Reg.WriteBinaryData(Value, PChar(s)^, Length(s));
        end else
          raise Exception.Create(SysErrorMessage(ERROR_CANTWRITE));
      except
        Reg.CloseKey;
        raise;
      end;
      Reg.CloseKey;
    end else
      raise Exception.Create(SysErrorMessage(GetLastError));
  finally
    Reg.Free;
  end;
end;

function IsRegistered: Boolean;
begin
  try
    GetRegistryData(HKEY_LOCAL_MACHINE, '\Software\NSiS\DS\', 'app_name');

    Result :=
      VartoStr(GetRegistryData(HKEY_LOCAL_MACHINE, '\Software\NSiS\DS\',
        'license_no')) = GenSerial(GetHardDiskSerial, ProjectCode);
  except
    Result := False;
  end;
end;

procedure DoRegister(MCode, InputKey: string);
begin
  if InputKey = GenSerial(MCode, ProjectCode) then
  begin
    SetRegistryData(HKEY_LOCAL_MACHINE, '\Software\NSiS\DS\', 'app_name',
      rdString, 'Digital Signage');
    SetRegistryData(HKEY_LOCAL_MACHINE, '\Software\NSiS\DS\', 'machine_code',
      rdString, MCode);
    SetRegistryData(HKEY_LOCAL_MACHINE, '\Software\NSiS\DS\', 'license_no',
      rdString, InputKey);

    Registered := True;
    MessageDlg('Thank you for registering our product.', mtConfirmation, [mbOK], 0);
  end
  else
  begin
    MessageDlg('License is incorrect.', mtError, [mbOK], 0);
  end;
end;

end.
